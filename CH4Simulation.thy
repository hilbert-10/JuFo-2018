theory CH4Simulation
  imports
    Main List
    CH4RegisterMachine
    CH4PositionalEncoding
begin

section {* Simuation of Register Machine Transitions by Equations *}
(* Lemmas stated by Marco David *)

(* [T] 4.6 *)
lemma lm04_06_one_step_relation_register:
  fixes l::register
    and ic::configuration
    and p::program
    and a::nat (* input *)
  defines "s \<equiv> fst ic"
      and "tape \<equiv> snd ic"

  defines "m \<equiv> length p"
      and "tape' \<equiv> snd (step ic p)"

  assumes "is_valid ic p a"
      and "terminates ic p q"

  (* Should be rephrased in terms of protocol *)
  shows "(tape'!l) = (tape!l) + (\<Sum>k<m. (if s = k \<and> isadd (p!k) \<and> l = goes_to (p!k) then 1 else 0))
                              - (\<Sum>k<m. (zero tape l) *
                                     (if s = k \<and> issub (p!k) \<and> l = goes_to (p!k) then 1 else 0))"
proof -
  show ?thesis sorry
qed

lemma lm04_07_one_step_relation_state:
  fixes d::state
    and c::configuration (* at fixed time t *)
    and p::program
    and a::nat (* input *)

  defines "m \<equiv> length p"
      and "tape \<equiv> snd c"
  defines "s \<equiv> state_column (fst c) m"
      and "s' \<equiv> state_column (fst (step c p)) m" (* t + 1 *)

  assumes "is_valid ic p a"
      and "terminates ic p q"

  shows "(s' ! d)  =   (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then s!k else 0))
                    +  (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then s!k && zero tape (modifies (p!k)) else 0))
                    +  (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then s!k && (1 - zero tape (modifies (p!k))) else 0))"
proof -
  show ?thesis sorry
qed


(* Next 3 lemmas use conversion from columns to rows, specifically need more assumptions:
 * - termination of register machine
 * - initial conditions
 * - one step relations
 *)

(* [T] 4.15 *)
lemma lm04_15_d_masks_registers:
  fixes l::register
    and ic::configuration
    and p::program
    and a::nat (* input *)
    and c::nat
  defines "s \<equiv> fst ic"
      and "tape \<equiv> snd ic"

  defines "m \<equiv> length p"
      and "tape' \<equiv> snd (step ic p)"

  assumes "is_valid ic p a"
      and "terminates ic p q"

  (* to be exported to a function that verifies this *)
  assumes "\<And>l t :: nat. (l < n & t \<le> q) \<longrightarrow> (2^c > rlt t l & 2^c > zlt t l)"
  assumes "\<And>k t :: nat. (k < m & t \<le> q) \<longrightarrow> (2^c > skt t k)"
  assumes "c > 0" (* needs to be proven from the above *)

  defines defB: "b \<equiv> B c"
      and "cl \<equiv> run ic p q"

  defines "d \<equiv> D q c b"
      and "rl \<equiv> RL q ((register_rows cl)!l) b"
    shows "masks (nat d) rl" (is "?P c")
proof -
  show ?thesis sorry
qed

(* [T] 4.17 *)
lemma lm04_17_masks_zeros:
  fixes c::nat
    and l::register (* or equivalently to zero-indicator values *)
    and cl::"configuration list" (* cannot be assumed, needs `run` fct from initial cond. *)
  defines "b \<equiv> B c"
      and "q \<equiv> length cl"
  defines "e \<equiv> E q b"
      and "zl \<equiv> ZL q ((zero_rows cl)!l) b"
  shows "masks e zl"
proof-
  show ?thesis sorry
qed

(* [T] 4.19 -- 4.20 *)
lemma lm04_19_relation_register_zeros:
  fixes c::nat
    and l::register
    and cl::"configuration list" (* cannot be assumed, needs `run` fct from initial cond. *)
  defines "b \<equiv> B c"
      and "q \<equiv> length cl"
  defines "f \<equiv> F q c b"
      and "d \<equiv> D q c b"
      and "zl \<equiv> ZL q ((zero_rows cl)!l) b"
      and "rl \<equiv> RL q ((register_rows cl)!l) b"
    shows "2^c * zl = (rl + nat d) && f"
proof -
  have "2^c * zl = (rl + 2^c - 1) && 2^c" sorry (* 4.19 *)
  then show ?thesis sorry (* 4.20 *)
qed

subsubsection {* Multiple-step relations *}

(* [T] 4.22 for registers *)
lemma lm04_22_multiple_register:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == length p"
  defines "rl == RL q ((register_rows cl)!l) b"
      and "zl \<equiv> ZL q ((zero_rows cl)!l) b"
      and "sk == \<lambda>k. SK q ((state_rows cl p)!k) b"
    shows "rl = b*rl + b * (\<Sum>k<m. (if isadd (p!k) \<and> l = modifies (p!k) then sk k else 0))
                     - b * (\<Sum>k<m. (if issub (p!k) \<and> l = modifies (p!k) then zl && sk k else 0))"
proof -
  fix k (* ? *)
  consider (add) "isadd (p!k)" | (sub) "issub (p!k)" | (halt) "issub (p!k)" sorry
  thus ?thesis sorry
qed

lemma lm04_23_multiple_register_1:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
    and a :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  assumes "l = 0"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == length p"
  defines "rl == RL q ((register_rows cl)!l) b"
      and "zl \<equiv> ZL q ((zero_rows cl)!l) b"
      and "sk == \<lambda>k. SK q ((state_rows cl p)!k) b"
    shows "rl =  a + b*rl + b * (\<Sum>k<m. (if isadd (p!k) \<and> l = modifies (p!k) then sk k else 0))
                          - b * (\<Sum>k<m. (if issub (p!k) \<and> l = modifies (p!k) then zl && sk k else 0))"
proof -
  fix k (* ? *)
  consider (add) "isadd (p!k)" | (sub) "issub (p!k)" | (halt) "issub (p!k)" sorry
  thus ?thesis sorry
qed

(* [T] 4.24 for states *)
lemma lm04_24_multiple_states:
  fixes c :: nat
    and d :: state
    and ic :: configuration
    and p :: program
    and q :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  assumes "d > 0"
  assumes "register_machine_check ic p = True"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == length p"
  defines "e == E q b"
  defines "sd == SK q ((state_rows cl p)!d) b"
      and "sk == \<lambda>k. SK q ((state_rows cl p)!k) b"
      and "zl == \<lambda>k. (ZL q ((zero_rows cl)!(modifies (p!k))) b)"
    shows "sd =   b * (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then sk k else 0))
                + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then sk k && zl k else 0))   
                + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then sk k && (e - zl k) else 0))"
proof -
  define sadd where "sadd \<equiv> (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then sk k else 0))"
  define ssub where "ssub \<equiv> (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then sk k && zl k else 0))"
  define salt where "salt \<equiv> (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then sk k && (e - zl k) else 0))"

  define skt where "skt \<equiv> \<lambda>t. digit sd b t" (* t-th digit of sd in base b *)
  define skt' where "skt' \<equiv> \<lambda>t. digit sd b (t+1)" (* t+1st digit of sd in base b *)
  define zlt where "zlt \<equiv> \<lambda>t k. digit (zl k) b t" (* t-th digit of zl in base b *)

  define a1 where "a1 \<equiv> \<lambda>t. (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then skt t && (zlt t k) else 0))"
  define a2 where "a2 \<equiv> \<lambda>t. (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then skt t && zlt t k else 0))"
  define a3 where "a3 \<equiv> \<lambda>t. (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then skt t && (1 - zlt t k) else 0))"

  from lm04_07_one_step_relation_state
  have sum: "\<And>t. skt' t = a1 t + a2 t + a3 t"
    sorry
  from sum have "\<And>t. b^Suc t * skt' t = b^Suc t * a1 t + b^Suc t * a2 t + b^Suc t * a3 t"
    by (simp add: distrib_left)
  from this have "(\<Sum>t<q. b^Suc t * skt' t) = (\<Sum>t<q. b^Suc t * a1 t + b^Suc t * a2 t + b^Suc t * a3 t)"
    by auto
  also from sum have "\<And>t t'::nat. b^Suc t * (skt' t + skt' t') = b^Suc t * (a1 t + a1 t' + a2 t + a2 t' + a3 t + a3 t')"
    by auto
  moreover have "b * sd = (\<Sum>t<q. b^Suc t * skt' t)" sorry
  (*ultimately have "b * s" *)

  (* Finally: *)
  have "sd = b * (sadd + ssub + salt)" sorry
  thus ?thesis
    by (simp add: sadd_def salt_def semiring_normalization_rules(34) ssub_def)
qed

(* [T] 4.25 for states *)
lemma lm04_25_multiple_states_1:
  fixes c :: nat
    and d :: state
    and ic :: configuration
    and p :: program
    and q :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  assumes "d = 0"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == length p"
  defines "e == E q b"
  defines "sd == SK q ((state_rows cl p)!d) b"
      and "sk == \<lambda>k. SK q ((state_rows cl p)!k) b"
    shows "sd = 1 +  b * (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then sk k else 0))
                  + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then sk k && (ZL q ((zero_rows cl)!(modifies (p!k))) b) else 0))   
                  + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then sk k && (e - (ZL q ((zero_rows cl)!(modifies (p!k))) b)) else 0))"
proof -
  fix k (* ? *)
  consider (add) "isadd (p!k)" | (sub) "issub (p!k)" | (halt) "issub (p!k)" sorry
  thus ?thesis sorry
qed

subsection {* Halting conditions / Wrapping up *}

(* [4.8] *)
(* THE HALTING CONDITION *)
lemma lm04_08_halting:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == (length p)"
  defines "s == state_rows cl p"
  shows "(s ! m ! q) = 1"
proof-
  show ?thesis sorry
qed

lemma lm04_09_halting:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
    and t :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == (length p)"
  defines "s == state_rows cl p"
  assumes y
  shows "t \<noteq> q --> (s ! m ! t) = 0"
proof-
  show ?thesis sorry
qed

lemma lm04_26_halting:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
    and t :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == (length p)"
  defines "s == state_rows cl p"
  assumes y
  defines "sm == (SK q (s ! m) b)"
  shows "sm = b^q"
proof-
  from sm_def cl_def lm04_09_halting m_def s_def terminate 
    have st1: "(ALL x < q. s ! m ! x = 0)" by auto
  from cl_def lm04_08_halting m_def s_def terminate 
    have st2: "(s ! m ! q) = 1" by blast
  from st1 st2 sm_def show ?thesis sorry
qed

end
