(* SECTION 4.4: POSITIONAL ENCODING OF THE PROTOCOL *)

theory CH4PositionalEncoding

imports
  Main
  CH4RegisterMachine
  CH2PositionalNotation CH2DigitComp
begin

(* Definitions implemented by Abhik Pal *)

(* [D] 4.11 *)
fun SK :: "nat \<Rightarrow> state list \<Rightarrow> nat \<Rightarrow> nat" where
  "(SK q skt b) = (\<Sum>t<q. skt!t * b^t)"

(* [D] 4.12 *)
fun RL :: "nat \<Rightarrow> register list \<Rightarrow> nat \<Rightarrow> nat" where
  "(RL q rlt b) = (\<Sum>t<q. rlt!t * b^t)"

(* [D] 4.13 *)
fun ZL :: "nat \<Rightarrow> nat list \<Rightarrow> nat \<Rightarrow> nat" where
  "(ZL q zlt b) = (\<Sum>t<q. zlt!t * b^t)"

(* [D] 4.14 *)
fun B :: "nat \<Rightarrow> nat" where
  "(B c) = 2^(Suc c)"

(* [D] 4.18 *)
fun E :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "(E q b) = (\<Sum>t<(Suc q). (b^t))"

(* [D] 4.21 *)
fun F :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat" where
  "(F q c b) = (\<Sum>t<(Suc q). (2^c) * (b^t))"

(* [D] 4.16 *)
fun D :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> int" where
  "(D q c b) = int (F q c b) - int (E q b)"

(* Used for Converse Proof *) 
(* [D] 4.28 *)
fun RLT :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat" where
  "(RLT rl b t) = (digit rl b t)"

(* [D] 4.29 *)
fun SKT :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat" where
  "(SKT sk b t) = (digit sk b t)"

(* [D] 4.30 *)
fun ZLT :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat" where
  "(ZLT zl b t) = (digit zl b t)"

end