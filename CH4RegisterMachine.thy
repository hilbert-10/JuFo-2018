theory CH4RegisterMachine
imports Main List
begin

(* Joint Work of Abhik Pal, Marco David, Scott Deng *)

subsection {* Basic Datatype Definitions *}

(* Type synonyms for registers (= register indices) the "tape" (sim. to a
 * Turing machine) that contains a list of register values.
 *)
type_synonym register = nat
type_synonym tape = "register list"

(* The register machine understands "instructions" that operate on state(-id)s
 * and modify register(-id)s. The machine stops at the HALT instruction.
 *)
type_synonym state = nat
datatype instruction =
  Add (modifies : register) (goes_to : state) |
  Sub (modifies : register) (goes_to : state) (goes_to_alt : state) |
  Halt

(* A program, then, just becomes a list of these instructions *)
type_synonym program = "instruction list"

(* A configuration of the (runtime of) a machine encodes information about the
 * instruction and the state of the registers (i.e., the tape). We describe it
 * here as a tuple.
 *)
type_synonym configuration = "(state * tape)"

subsection {* Essential Functions to operate the Register Machine *}

(* Given a tape of register values and some instruction(s) the register
 * machine first reads the value of the register from the tape (by convention
 * assume that the value "read" by the HALT state is zero). The machine then,
 * fetches the next instruction from the program, and finally updates the
 * tape to reflect changes by the last instruction.
 *)
fun read :: "tape \<Rightarrow> instruction \<Rightarrow> nat"
  where
    "(read t (Add r _)) = t!r"
  | "(read t (Sub r _ _)) = t!r"
  | "(read t Halt) = 0"

fun fetch :: "program \<Rightarrow> state \<Rightarrow> instruction \<Rightarrow> nat \<Rightarrow> state"
  where
    "(fetch p s (Add r next) _) = next"
  | "(fetch p s (Sub r next nextalt) val) = (if val = 0 then nextalt else next)"
  | "(fetch p s Halt _) = s"

fun update :: "tape \<Rightarrow> instruction \<Rightarrow> tape"
  where
    "(update t (Add r _)) = (list_update t r (t!r + 1))"
  | "(update t (Sub r _ _)) = (list_update t r (if (t!r = 0)
                                                then 0 else (t!r) - 1))"
  | "(update t Halt) = t"

(* Finally, to "run" the machine we describe a function that --- given the
 * current state of the machine as a configuration and the program --- lets us
 * "step" to the next configuration of the machine.
 *
 * We then use this to get a function that does multiple steps of the machine
 * (while keeping track of its execution history).
 *)
fun step :: "configuration \<Rightarrow> program \<Rightarrow> configuration"
  where
    "(step (s, t) p) = (let nexts = fetch p s (p!s) (read t (p!s));
                            nextt = update t (p!s)
                        in (nexts, nextt))"
(* p!s = program index state = instruction *)

fun steps :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> configuration"
  where
    "(steps c p 0) = c"
  | "(steps c p (Suc n)) = (steps (step c p) p n)"

subsection {* Auxiliary Functions for Proofs *}

(* Zero Function for tape and register *)
fun zero :: "tape \<Rightarrow> register \<Rightarrow> nat"
  where
    "zero t l = (if 0 = t!l then 0 else 1)"

(* Check for Instruction Type *)
fun isadd :: "instruction \<Rightarrow> bool"
  where
    "isadd (Add _ _) = True"
  | "isadd _ = False"

fun issub :: "instruction \<Rightarrow> bool"
  where
    "issub (Sub _ _ _) = True"
  | "issub _ = False"

fun ishalt :: "instruction \<Rightarrow> bool"
  where
    "ishalt Halt = True"
  | "ishalt _ = False"

subsection {* From Configurations to a Protocol *}

subsubsection {* State Values *}

(* Function that maps configurations into a proper protocol column.
 * The first argument is the current state (index with entry 1)
 * The second argument is the length of the program (total number of states) *)
fun state_column :: "state \<Rightarrow> nat \<Rightarrow> nat list"
  where
    "state_column i m = list_update (replicate 0 m) i 1"

(* Returns a list of columns of states (first index t, second index k) *)
fun stk :: "configuration list \<Rightarrow> program \<Rightarrow> state list list" where
  "stk [] p = [[]]" |
  "stk ((s, _) # configlist) p = (state_column s (length p)) # (stk configlist p)"

(* transpose the list of columns into a list of rows: theory List function transpose *)
(* square grid of lists is given by constant length p *)
fun state_rows :: "configuration list \<Rightarrow> program \<Rightarrow> state list list" where
  "state_rows c p = transpose (stk c p)"

subsubsection {* Register Values *}

fun rtl :: "configuration list \<Rightarrow> register list list" where
  "rtl [] = [[]]" |
  "rtl ((_, t) # configlist) = t # (rtl configlist)" 

fun register_rows :: "configuration list \<Rightarrow> register list list" where
  "register_rows c = transpose (rtl c)"

subsubsection {* Zero-Indicator Values *}

fun ztl :: "configuration list \<Rightarrow> register list list" where
  "ztl [] = [[]]" |
  "ztl ((_, t) # configlist) = (map (\<lambda>x. (if x = 0 then 0 else 1)) t) # (ztl configlist)"

fun zero_rows :: "configuration list \<Rightarrow> register list list" where
  "zero_rows c = transpose (ztl c)"

subsubsection {* Run Function *}

(* given an initial configuration, execute `step` until in halt state
 * return list of all configurations (length = q) *)
(* impossible to implement, because termination order requires termination of the RM *)
(* is given maximal length via parameter q :: nat -- implicit assumption of termination *)
fun run :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> configuration list"
  where
    "run c _ 0 = [c]" |
    "run (s, t) p (Suc q) = (if (p!s = Halt) then [(s, t)] else (s, t) # (run (step (s, t) p) p) q)"


subsection {* Validity Checks and Assumptions *}
(* Jointly Implemented by Scott Deng and Marco David *)

subsubsection {* Helper Functions *}

(* check bound for each type of instruction *)
(* take a m representing the upper bound for state number *)
fun instruction_state_check :: "nat \<Rightarrow> instruction \<Rightarrow> bool"
  where "instruction_state_check _ Halt = True"
  |     "instruction_state_check m (Add _ ns) = (ns < m)"
  |     "instruction_state_check m (Sub _ ns1 ns2) = ((ns1 < m) & (ns2 < m))"

fun instruction_register_check :: "nat \<Rightarrow> instruction \<Rightarrow> bool"
  where "instruction_register_check _ Halt = True"
  |     "instruction_register_check m (Add n _) = (n < m)"
  |     "instruction_register_check m (Sub n _ _) = (n < m)"

(* passes function via currying into list_all *)
fun program_state_check :: "program \<Rightarrow> bool"
  where "program_state_check p = list_all (instruction_state_check (length p)) p"

fun program_register_check :: "program \<Rightarrow> nat \<Rightarrow> bool"
  where "program_register_check p n = list_all (instruction_register_check n) p"

fun tape_check :: "tape \<Rightarrow> nat \<Rightarrow> bool"
  where "tape_check (x # xs) a = ((x = a) & list_all (\<lambda>x. x = 0) xs)"
  |     "tape_check [] _ = False"

fun program_includes_halt :: "program \<Rightarrow> bool"
  where "program_includes_halt [] = False"
  | "program_includes_halt p = (p!(length p - 1) = Halt)"

subsubsection {* Is Valid & Terminates *}

(* Final Validity Check / Assumption of Initial Configuration & Program *)
fun is_valid :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> bool"
  where "is_valid (0 , t) p a = 
           ((program_state_check p) 
         &  (program_register_check p (length t))
         &  (program_includes_halt p)
         &  (tape_check t a))"
  | "is_valid _ _ _ = False"

fun terminates :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> bool"
  where "terminates c p q = (length p = fst (steps c p q))"
end