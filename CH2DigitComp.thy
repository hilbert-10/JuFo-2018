(*
 * 2.6 
 * DIGIT BY DIGIT COMPARISON OF NATURAL NUMBERS
 * Helper Methods
 *)

theory CH2DigitComp
imports Main CH2PositionalNotation

begin

(* element wise product of two lists. *)
fun hl_list_prod :: "nat list => nat list => nat list"
  where
    "(hl_list_prod [] []) = [0]"
  | "(hl_list_prod as []) = [0]"
  | "(hl_list_prod [] bs) = [0]"
  | "(hl_list_prod (a # as) (b # bs)) = (a * b) # (hl_list_prod as bs)"

(* mimics binary bitwise and *)
fun digit_binary_mult :: "nat => nat => nat" (infixl "&&" 70)
  where
    "(digit_binary_mult a b) = (pnval (Pos 2 (hl_list_prod
                                              (hl_digit_list a 2)
                                              (hl_digit_list b 2))))"

fun orthogonal :: "nat => nat => bool"
  where
    "(orthogonal a b) = (if ((digit_binary_mult a b) = 0) then True else False)"

(* checks if a >= b for a in as and b in bs *)
fun hl_list_comp :: "nat list => nat list => bool list"
  where
    "(hl_list_comp [] []) = [True]"
  | "(hl_list_comp as []) = [True]"
  | "(hl_list_comp [] bs) = [False]"
  | "(hl_list_comp (a # as) (b # bs)) = (a >= b) # (hl_list_comp as bs)"

(* a "masks" b iff a >= b digit wise in binary *)
fun masks :: "nat => nat => bool" (infixl "msk" 70)
  where
    "(masks a b) = (fold (%x y. x & y)
                         (hl_list_comp (hl_digit_list a 2) (hl_digit_list b 2))
                         True)"

end